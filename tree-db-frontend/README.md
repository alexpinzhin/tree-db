# tree-db-frontend

Разработан с помощью Vue 3

## Как запустить проект

### Вариант 1. Скомпилировать и запустить в development-режиме
```
npm install
npm run serve

Приложение будет доступно по ссылке http://localhost:8080
```

### Вариант 2. Запустить готовую сборку из /dist
```
npm install -g serve
serve -s dist
Приложение будет доступно по ссылке http://localhost:5000
```


