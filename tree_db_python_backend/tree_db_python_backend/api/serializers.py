from django.contrib.auth.models import User, Group
from .models import NodeWithChildren
from rest_framework import serializers


# class NodeWithChildrenSerializer(serializers.Serializer):
#     id = serializers.CharField()
#     value = serializers.CharField()
#     parentId = serializers.CharField()
#     deleted = serializers.BooleanField()
#     children = serializers.ListField(child = serializers.CharField())
#
#     def update(self, instance, validated_data):
#         pass
#
#     def create(self, validated_data):
#         pass

class NodeWithChildrenSerializer(serializers.ModelSerializer):
    # id = serializers.CharField()
    # value = serializers.CharField()
    # parentId = serializers.CharField()
    # deleted = serializers.BooleanField()
    # children = serializers.ListField(child = serializers.CharField())

    class Meta:
        model = NodeWithChildren
        fields = ('id', 'value')


# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ['url', 'username', 'email', 'groups']
#
#
# class GroupSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ['url', 'name']