from django.shortcuts import render

from django.contrib.auth.models import User, Group

from django.views import View
from django.http import JsonResponse
import json
from .models import NodeWithChildren
from .serializers import NodeWithChildrenSerializer
from .treeDbRepository import TreeDbRepository
from django.core import serializers


class DatabaseView(View):
    #repository = TreeDbRepository()

    def post(self, request):
        data = json.loads(request.body.decode("utf-8"))
        p_name = data.get('product_name')
        p_price = data.get('product_price')
        p_quantity = data.get('product_quantity')

        product_data = {
            'product_name': p_name,
            'product_price': p_price,
            'product_quantity': p_quantity,
        }

        # cart_item = CartItem.objects.create(**product_data)
        #
        # data = {
        #     "message": f"New item added to Cart with id: {cart_item.id}"
        # }
        return JsonResponse(data, status=201)

    def get(self, request):
        repository = TreeDbRepository()
        allNodes = repository.loadAllNodes()

        # items_data = []
        # for item in items:
        #     items_data.append({
        #         'product_name': item.product_name,
        #     })
        # data = serializers.serialize("json", [allNodes])

        data = NodeWithChildrenSerializer(allNodes).data
        # data = {
        #     'items': allNodes
        # }

        return JsonResponse(data)
