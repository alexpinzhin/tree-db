from .models import NodeWithChildren


class TreeDbRepository:
    rootNode: None

    def __init__(self):
        self.reset()

    def reset(self):
        self.rootNode = NodeDbRecord(1, "Node 1")
        self.rootNode.add_children(
            [
                NodeDbRecord(2, "Node 2"),
                NodeDbRecord(3, "Node 3")
            ]
        )

    def loadAllNodes(self):
        return self.loadChildNodesRecursive(self.rootNode)

    def loadChildNodesRecursive(self, nodeDbRecord):
        node = NodeWithChildren(
            nodeDbRecord.id,
            nodeDbRecord.value,
            nodeDbRecord.parent.id if nodeDbRecord.parent else None,
            nodeDbRecord.deleted)
        for childNode in nodeDbRecord.children:
            node.addChild(self.loadChildNodesRecursive(childNode))
        return node


class NodeDbRecord:
    def __init__(self, identifier, value):
        self.id = identifier
        self.value = value

    parent = None
    deleted = False
    children = []

    def add_children(self, children):
        for child in children:
            child.parent = self
        self.children = children
        return self
