import json


class NodeWithChildren:

    def __init__(self, identifier, value, parentId, deleted):
        self.id = identifier
        self.value = value
        self.parentId = parentId
        self.deleted = deleted

    children = []

    def addChild(self, node):
        self.children.append(node)


