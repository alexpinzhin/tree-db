# tree-db-java-backend

Требования: Java 11

## Как запустить проект

Собрать проект с помощью команды

```
mvn clean install
```

Запустить проект

```
java -jar target/tree-db-java-backend-0.0.1-SNAPSHOT.jar
```

По умолчанию сервер слушает порт 8081