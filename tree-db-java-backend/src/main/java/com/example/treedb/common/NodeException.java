package com.example.treedb.common;

public class NodeException extends Exception {

    public NodeException(String message) {
        super(message);
    }
}
