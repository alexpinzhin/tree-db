package com.example.treedb.repository;

import com.example.treedb.services.model.Node;
import com.example.treedb.services.model.NodeWithChildren;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class TreeDbRepository {

    private NodeDbRecord rootNode;

    @PostConstruct
    public void init() {
        reset();
    }

    public void reset() {
        rootNode = new NodeDbRecord("1", "Node 1").addChildren(
                new NodeDbRecord("2", "Node 2"),
                new NodeDbRecord("3", "Node 3").addChildren(
                        new NodeDbRecord("4", "Node 4", true),
                        new NodeDbRecord("5", "Node 5").addChildren(
                                new NodeDbRecord("7", "Node 7"),
                                new NodeDbRecord("8", "Node 8")
                        ),
                        new NodeDbRecord("6", "Node 6")));
    }

    public void createNode(Node treeNode) {
        NodeDbRecord node = findNodeByIdRecursive(treeNode.getParentId(), rootNode);
        if (node != null) {
            node.addChildren(new NodeDbRecord(treeNode.getId(), treeNode.getValue()));
        }
    }

    public void updateNodeValue(String id, String value) {
        NodeDbRecord node = findNodeByIdRecursive(id, rootNode);
        if (node != null) {
            node.setValue(value);
        }
    }

    public void deleteNode(String id) {
        NodeDbRecord node = findNodeByIdRecursive(id, rootNode);
        if (node != null) {
            deleteRecursive(node);
        }
    }

    private void deleteRecursive(NodeDbRecord node) {
        node.setDeleted(true);
        node.getChildren().forEach(this::deleteRecursive);
    }

    public Node findNodeById(String id) {
        NodeDbRecord node = findNodeByIdRecursive(id, rootNode);
        if (node != null) {
            return new Node(node.getId(), node.getValue(), node.getParent() != null ? node.getParent().getId() : null, node.isDeleted());
        } else {
            return null;
        }
    }

    private NodeDbRecord findNodeByIdRecursive(String id, NodeDbRecord node) {
        if (node.getId().equals(id) && !node.isDeleted()) {
            return node;
        } else {
            for (NodeDbRecord childNode: node.getChildren()) {
                NodeDbRecord found = findNodeByIdRecursive(id, childNode);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }
    }

    public NodeWithChildren loadAllNodes() {
        return loadChildNodesRecursive(rootNode);
    }

    public NodeWithChildren loadChildNodesRecursive(NodeDbRecord nodeDbRecord) {
        NodeWithChildren node = new NodeWithChildren(
                nodeDbRecord.getId(),
                nodeDbRecord.getValue(),
                nodeDbRecord.getParent() != null ? nodeDbRecord.getParent().getId() : null,
                nodeDbRecord.isDeleted());
        nodeDbRecord.getChildren().forEach(childNode -> node.addChild(loadChildNodesRecursive(childNode)));
        return node;
    }

    /**
     * Класс для внутреннего представления записи БД
     */
    @Data
    @AllArgsConstructor
    private static class NodeDbRecord {
        private String id;
        private String value;
        private NodeDbRecord parent;
        private List<NodeDbRecord> children = new ArrayList<>();

        private boolean deleted = false;

        public NodeDbRecord(String id, String value) {
            this.id = id;
            this.value = value;
        }

        public NodeDbRecord(String id, String value, boolean deleted) {
            this.id = id;
            this.value = value;
            this.deleted = deleted;
        }

        public NodeDbRecord addChildren(NodeDbRecord... children) {
            for (NodeDbRecord child : children) {
                child.setParent(this);
                this.children.add(child);
            }
            return this;
        }
    }

}


