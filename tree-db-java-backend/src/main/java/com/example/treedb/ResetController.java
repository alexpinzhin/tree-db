package com.example.treedb;

import com.example.treedb.services.CacheService;
import com.example.treedb.services.DatabaseService;
import com.example.treedb.services.model.NodeWithChildren;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class ResetController {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private CacheService cacheService;

    @PostMapping(path = "/reset")
    public void reset() {
        databaseService.reset();
        cacheService.reset();
    }
}
