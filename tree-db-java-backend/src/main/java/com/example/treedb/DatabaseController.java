package com.example.treedb;

import com.example.treedb.services.CacheService;
import com.example.treedb.services.DatabaseService;
import com.example.treedb.services.model.NodeWithChildren;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/db")
public class DatabaseController {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private CacheService cacheService;

    @GetMapping(path = "/nodes")
    public NodeWithChildren loadAllNodes() {
        return databaseService.loadAllNodes();
    }

}
