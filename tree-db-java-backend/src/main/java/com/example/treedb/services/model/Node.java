package com.example.treedb.services.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Node {

    private String id;

    private String value;

    private String parentId;

    private boolean deleted;

}
