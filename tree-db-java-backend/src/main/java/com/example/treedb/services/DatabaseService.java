package com.example.treedb.services;

import com.example.treedb.common.NodeException;
import com.example.treedb.repository.TreeDbRepository;
import com.example.treedb.services.model.CachedNode;
import com.example.treedb.services.model.Node;
import com.example.treedb.services.model.NodeWithChildren;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class DatabaseService {

    @Autowired
    private TreeDbRepository treeDbRepository;

    public NodeWithChildren loadAllNodes() {
        return treeDbRepository.loadAllNodes();
    }

    public void reset() {
        treeDbRepository.reset();
    }
}
