package com.example.treedb.services.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CachedNode { // todo inheritance, pattern, adaptor

    private Node node;

    private boolean saved;

    private boolean markedForDelete;

    private boolean markedForUpdate;

    private String parentId;

    private List<CachedNode> children = new ArrayList<>();

    public CachedNode(Node node, boolean saved) {
        this.node = node;
        this.saved = saved;
    }

    public void addChild(CachedNode node) {
        children.add(node);
        node.setParentId(this.node.getId());
    }


}
