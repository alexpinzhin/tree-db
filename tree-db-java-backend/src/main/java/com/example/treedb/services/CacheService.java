package com.example.treedb.services;

import com.example.treedb.common.NodeException;
import com.example.treedb.services.model.CachedNode;
import com.example.treedb.services.model.Node;
import com.example.treedb.repository.TreeDbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class CacheService {
    
    private List<CachedNode> cache = new ArrayList<>();

    @Autowired
    private TreeDbRepository treeDbRepository;

    public List<CachedNode> getCachedNodes() {
        return cache;
    }

    public void loadNode(String id) throws NodeException {
        CachedNode existingCachedNode = findCachedNodeById(id);
        if (existingCachedNode != null) {
            throw new NodeException("Node \"" + existingCachedNode.getNode().getValue()  + "\" already loaded");
        }

        Node node = treeDbRepository.findNodeById(id);
        if (node == null || node.isDeleted()) {
            throw new NodeException("Cannot load node - deleted or doesn't exist");
        }

        CachedNode cachedNode = new CachedNode(node, true);

        CachedNode parent = node.getParentId() != null ? findCachedNodeById(node.getParentId()) : null;
        if (parent != null) {
            parent.addChild(cachedNode);
            // помечаем загруженную ноду на удаление, если ее старшая нода была помечена на удаление
            if (parent.isMarkedForDelete()) {
                cachedNode.setMarkedForDelete(true);
            }
        } else {
            cache.add(cachedNode);
        }
        reattachChildNodes(cachedNode);

        // распространяем markedForDelete для всех младших нодов ранее загруженные в кэш без непосредственной связи
        if (cachedNode.isMarkedForDelete()) {
            propagateMarkedForDeleteToChildNodes(cachedNode);
        }
    }

    /**
     * Переподключает ноды ранее загруженные в кэш без непосредственной связи
     */
    private void reattachChildNodes(CachedNode cachedNode) {
        Iterator<CachedNode> iterator = cache.iterator();
        while (iterator.hasNext()) {
            CachedNode cachedRootNode = iterator.next();
            if (cachedRootNode.getNode().getParentId() != null && cachedRootNode.getNode().getParentId().equals(cachedNode.getNode().getId())) {
                cachedNode.addChild(cachedRootNode);
                iterator.remove();
            }
        }
    }

    public void addNode(String parentId, String value) throws NodeException {
        if (parentId == null) {
            throw new NodeException("Cannot add root node, parent node must be provided");
        }
        CachedNode parent = findCachedNodeById(parentId);

        if (parent.isMarkedForDelete()) {
            throw new NodeException("Cannot add child node to parent node marked for delete");
        }

        CachedNode node = new CachedNode(new Node(UUID.randomUUID().toString(), value, parentId, false),
                false);
        parent.addChild(node);
    }

    public void updateNodeValue(String id, String value) {
        CachedNode node = findCachedNodeById(id);
        if (node != null) {
            node.setMarkedForUpdate(true);
            node.getNode().setValue(value);
        }
    }

    public void markNodeForDeletion(String id) {
        CachedNode cachedNode = findCachedNodeById(id);

        if (!cachedNode.isSaved()) {  // небольшая оптимизация - если нода еще не сохранена в БД, то удаляем ее из кэша
            if (cachedNode.getParentId() == null) {
                cache.remove(cachedNode);
            } else {
                CachedNode parent = findCachedNodeById(cachedNode.getParentId());
                parent.getChildren().remove(cachedNode);
                cachedNode.setParentId(null);
            }
        } else { // иначе помечаем на удаление из БД
            cachedNode.setMarkedForDelete(true);
        }

        propagateMarkedForDeleteToChildNodes(cachedNode);
    }

    public void propagateMarkedForDeleteToChildNodes(CachedNode node) {
        Iterator<CachedNode> iterator = node.getChildren().iterator();
        while (iterator.hasNext()) {
            CachedNode childNode = iterator.next();
            propagateMarkedForDeleteToChildNodes(childNode);
            if (!childNode.isSaved()) {  // небольшая оптимизация - если нода еще не сохранена в БД, то удаляем ее из кэша
                iterator.remove();
                childNode.setParentId(null);
            } else { // иначе помечаем на удаление из БД
                childNode.setMarkedForDelete(true);
            }
        }

    }
    
    public void saveChanges() {
        saveToDbRecursive(cache.iterator());
    }

    private void saveToDbRecursive(Iterator<CachedNode> iterator) {
        while (iterator.hasNext()) {
            CachedNode node = iterator.next();
            if (!node.isSaved()) {
                treeDbRepository.createNode(node.getNode());
                node.setSaved(true);
            }

            saveToDbRecursive(node.getChildren().iterator());

            if (node.isMarkedForDelete()) {
                treeDbRepository.deleteNode(node.getNode().getId());
                iterator.remove();
                node.setParentId(null);
            } else if (node.isMarkedForUpdate()) {
                treeDbRepository.updateNodeValue(node.getNode().getId(), node.getNode().getValue());
                node.setMarkedForUpdate(false);
            }
        }
    }

    public CachedNode findCachedNodeById(String id) {
        for (CachedNode root : cache) {
            CachedNode node = findCachedNodeByIdRecursive(id, root);
            if (node != null) {
                return node;
            }
        }
        return null;
    }

    private CachedNode findCachedNodeByIdRecursive(String id, CachedNode node) {
        if (node.getNode().getId().equals(id)) {
            return node;
        } else {
            for (CachedNode childNode: node.getChildren()) {
                CachedNode found = findCachedNodeByIdRecursive(id, childNode);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }
    }

    public void reset() {
        cache.clear();
    }
}
