package com.example.treedb.services.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class NodeWithChildren {

    private String id;

    private String value;

    private String parentId;

    private boolean deleted;

    public NodeWithChildren(String id, String value, String parentId, boolean deleted) {
        this.id = id;
        this.value = value;
        this.parentId = parentId;
        this.deleted = deleted;
    }

    private List<NodeWithChildren> children = new ArrayList<>();

    public void addChild(NodeWithChildren node) {
        children.add(node);
    }
}
