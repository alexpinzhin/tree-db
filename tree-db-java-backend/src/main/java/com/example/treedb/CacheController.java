package com.example.treedb;

import com.example.treedb.common.NodeException;
import com.example.treedb.services.CacheService;
import com.example.treedb.services.DatabaseService;
import com.example.treedb.services.model.CachedNode;
import com.example.treedb.services.model.NodeWithChildren;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/cache")
public class CacheController {

    @Autowired
    private CacheService cacheService;

    @GetMapping(path = "/nodes/{id}")
    public List<CachedNode> loadNode(@PathVariable String id) throws NodeException {
        cacheService.loadNode(id);
        return cacheService.getCachedNodes();
    }

    @PostMapping(path = "/nodes/{parentId}")
    public List<CachedNode> addNode(@PathVariable String parentId, @RequestBody String value) throws NodeException {
        cacheService.addNode(parentId, value);
        return cacheService.getCachedNodes();
    }

    @PutMapping(path = "/nodes/{id}")
    public List<CachedNode> updateNodeValue(@PathVariable String id, @RequestBody String value) {
        cacheService.updateNodeValue(id, value);
        return cacheService.getCachedNodes();
    }

    @DeleteMapping(path = "/nodes/{id}")
    public List<CachedNode> markNodeForDeletion(@PathVariable String id) {
        cacheService.markNodeForDeletion(id);
        return cacheService.getCachedNodes();
    }

    @PostMapping(path = "/nodes/apply")
    public List<CachedNode> apply() {
        cacheService.saveChanges();
        return cacheService.getCachedNodes();
    }

}
