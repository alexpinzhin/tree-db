# tree-db-java-backend

Требования: Java 11

## Как запустить проект

Собрать проект с помощью команды

```
mvn clean install
```

Запустить проект

```
java -jar target/tree-db-java-backend-0.0.1-SNAPSHOT.jar
```

По умолчанию сервер слушает порт 8081


# tree-db-frontend

Разработан с помощью Vue 3

## Как запустить проект

### Вариант 1. Скомпилировать и запустить в development-режиме
```
npm install
npm run serve

Приложение будет доступно по ссылке http://localhost:8080
```

### Вариант 2. Запустить готовую сборку из /dist
```
npm install -g serve
serve -s dist
Приложение будет доступно по ссылке http://localhost:5000
```


